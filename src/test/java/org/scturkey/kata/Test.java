package org.scturkey.kata;

import org.junit.Assert;

import java.util.List;

/**
 * @author mabulgu
 */
public class Test
{
    /**
     * Scenarios
     */

    @org.junit.Test
    public void test_frame_rolls_count()
    {
        Game game = new Game();

        Frame frame = new Frame();
        int rollCount = frame.getMaxRollCount();
        List<Roll> rolls = frame.getRolls();

        for(int i = 0; i < rollCount; i++)
        {
            Roll roll = game.roll();
            rolls.add(roll);
        }

        Assert.assertEquals(2, rolls.size());
    }

}
