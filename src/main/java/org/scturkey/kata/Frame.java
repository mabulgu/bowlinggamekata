package org.scturkey.kata;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mabulgu
 */
public class Frame
{
    private static final int MAX_ROLL_COUNT = 2;
    private List<Roll> rolls;

    public Frame()
    {
        this.rolls = new ArrayList<Roll>();
    }

    public int getMaxRollCount()
    {
        return MAX_ROLL_COUNT;
    }

    public List<Roll> getRolls()
    {
        return rolls;
    }

    public void setRolls(List<Roll> rolls)
    {
        this.rolls = rolls;
    }
}
